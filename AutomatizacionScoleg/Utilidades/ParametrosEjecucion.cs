﻿using System.Configuration;

namespace AutomatizacionScoleg.Utilidades
{
    public static class ParametrosEjecucion
    {
        #region "Variables de la Clase"
        //Establecer url de ambiente desde app.config.
        public static string RutaDelSitio = ConfigurationManager.AppSettings["QA"];

        //Establecer navegador desde app.config.
        public static string Navegador = ConfigurationManager.AppSettings["Navegador"];

        //Establecer valores de inicio de sesión
        public const string Usuario = "17.464.496-9";
        public const string Password = "1234567890";

        //Establecer string de conexión a base de datos desde app.config.
        public static string CadenaConexionBD = ConfigurationManager.ConnectionStrings["QA"].ConnectionString;

        #endregion
    }
}
